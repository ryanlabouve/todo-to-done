## Testing

```bash
npm install -g .
todo-to-done
```

## Development 

build and run Dockerfile
`docker build -t ryanlabouve/todo-to-done -f Dockerfile .`

## Deploy image 

docker tag todo-to-done 

docker push ryanlabouve/todo-to-done:tagname